﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fundamentos_de_C_sharp_ejemplo.models;

namespace Fundamentos_de_C_sharp_ejemplo
{
    internal class Program
    {
        public static void Main(String[] args)
        {
            // tipos de datos primitvos 
            // subtipos enteros
            byte numeroPequenio = 128;
            short numeroMediano = 32767;
            //2.147.483.647
            int numero = 2147483647;
            long numeroGigante = 9223372036854775807;
            //reglas especiales
            sbyte numeroPequenioNegativo = -128;
            ushort numeroMedianoPositivo = 65535;
            //4.294.967.295
            uint numeroPositivo = 4294967295;
            ulong numeriGigantePositivo = 18446744073709551615;

            // subtipos de punto flotante
            // cuidado con las conversiones
            // de 6 a 9 digitos
            float puntoFlotantePequeño = 123478.1323f;
            
            // de 15 a 17 digitos 
            double puntoFlotanteMediano = 10000000.10000d;
            // 28 a 29 díogitos
            decimal puntoFlotanteGrande = 128123123123132.9123123123123m;

            // subtipo caracter 
            char caracter = 'A';

            bool verdaderoFalseo = true;

            //tipos de datos compuestos 
            string cadena = "Hola Mundo";

            DateTime fecha = new DateTime();

            object anonimos = new{ nombre = "Pedro",  };

            // var 
            var variableVar = "Hola mundo";

            //Console.WriteLine(caracter.GetType() == typeof(System.Boolean));

            // Clases y Objetos

            Bebida b = new Bebida("Coca Cola", 1000);

            Cerveza c = new Cerveza("Rubia", 10);

            Cerveza c2 = new Cerveza("Rubia", 10, 500);

            Console.WriteLine(c.ToString());

            // Arreglos y Listas

            int [] numeros = new int[5] { 1,2,3,4,5};
                
            //indices 
            int num = numeros[0];

            //recorrer 
            //for 
            for (int i = 0; i < numeros.Length; i++)
            {
                Console.WriteLine(numeros[i]);
            }

            //foreach
            foreach(int nu in numeros)
            {
                Console.WriteLine(nu);
            }

            //Listas 
            List<int> numeros2 = new List<int>();
            numeros2.Add(1);
            numeros2.Add(2);
            numeros2.Add(3);
            numeros2.Remove(2);

            foreach (int nu in numeros2)
            {
                Console.WriteLine(nu);
            }

            List<Cerveza> cervezas = new List<Cerveza>() { new Cerveza("Cerveza Premium"), new Cerveza("Cerveza de Trigo") };

            foreach (Cerveza cv in cervezas){
                Console.WriteLine(cv.ToString());
            }

            cervezas.Add(new Cerveza("Cerveza Alemana"));

            foreach (Cerveza cv in cervezas)
            {
                Console.WriteLine(cv.ToString());
            }

            //Botilleria 
            Vino santaElena = new Vino("Santa Elena", 100, 1000);

            MostrarRecomendacion(santaElena);
            MostrarRecomendacion(c);


        }

        static void MostrarRecomendacion(IBebidaAlcoholica bebida)
        {
            Console.WriteLine($"La cantidad máxima recomendada es de {bebida.CantidadMaximaSugerida()}");
        }
    }
}

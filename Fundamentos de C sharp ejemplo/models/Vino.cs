﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fundamentos_de_C_sharp_ejemplo.models
{
    public class Vino : Bebida, IBebidaAlcoholica
    {
        public byte GradosAlcohol { get; set; }

        public Vino(string nombre, byte gradosAlcohol, short cantidadML = 500) : base(nombre, cantidadML)
        {
            this.GradosAlcohol = gradosAlcohol;
        }

        public int CantidadMaximaSugerida()
        {
            if (GradosAlcohol >= 2)
            {
                return 2;
            }
            else
            {
                return 6;
            }
        }
    }
}

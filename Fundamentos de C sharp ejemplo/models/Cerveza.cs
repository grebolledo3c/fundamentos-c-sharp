﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fundamentos_de_C_sharp_ejemplo.models
{
    public class Cerveza : Bebida, IBebidaAlcoholica
    {
        public byte GradosAlcohol { get; set; }

        public Cerveza(string nombre, byte gradosAlcohol = 5, short cantidadML = 200) : base(nombre, cantidadML)
        {
            this.GradosAlcohol = gradosAlcohol;
        }

        public int CantidadMaximaSugerida()
        {
            if(GradosAlcohol >= 10)
            {
                return 2;
            }
            else
            {
                return 4;
            }
        }
    }
}

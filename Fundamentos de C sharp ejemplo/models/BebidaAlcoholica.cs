﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fundamentos_de_C_sharp_ejemplo.models
{
    interface IBebidaAlcoholica
    {
        public byte GradosAlcohol { get; set; }

        public int CantidadMaximaSugerida();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fundamentos_de_C_sharp_ejemplo.models
{
    public class Bebida
    {
        public Bebida(string nombre, short cantidadML)
        {
            Nombre = nombre;
            CantidadML = cantidadML;
        }

        public string Nombre { get; set; }

        public short CantidadML { get; set; }

        public void Beberse(short cuantoBebio)
        {
            this.CantidadML = cuantoBebio;
        }

        public override string ToString()
        {
            return string.Format("[Nombre: {0}, Cantidad {1}]", this.Nombre, this.CantidadML.ToString());
        }

    }
}
